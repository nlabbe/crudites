import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { ApolloProvider } from 'react-apollo';
import client from './client';

import theme from './Theme';
import { ThemeProvider } from 'styled-components';

ReactDOM.render(
  <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <App client={client}/>
    </ThemeProvider>
  </ApolloProvider>, document.getElementById('root'));
registerServiceWorker();
