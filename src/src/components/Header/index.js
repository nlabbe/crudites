import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { consumerScroll } from '../../Context/scroll';

const Wrapper = styled.div`
  position: relative;
  z-index: 2;
  top: 0;
  left: 0;
  width: 100%;
  height: ${props => props.height}px;
`;

const Fixed = styled.div`
  top: 0px;
  left: 0px;
  width: 100%;
  overflow: hidden;
  position: fixed;
  height: ${props => props.height}px;
`;

const Absolute = styled.div`
  position: absolute;
  top: 0px;
  left: 50%;
  height: 100%;
  width: calc(100% + ${props => props.maxRadius}px);
  border-radius: 0px 0px ${props => props.radius}px ${props => props.radius}px;
  transform: translate(-50%, ${props => props.top}px);
  transition: all 0.3s ease;
  overflow: hidden;
  background: ${props => props.theme.color.primary};
  background: linear-gradient(to right, ${props => props.theme.color.primary} 0%, ${props => props.theme.color.secondary} 100%);
`;

const WrapperChildren = styled.div`
  position: absolute;
  bottom: 0px;
  left: 50%;
  transform: translate(-50%, ${props => props.top}px);
  transition: transform 0.3s ease;
  height: ${props => props.height}px;
  z-index: 1;
  width: calc(100% - 40px);
  background: white;
  border-radius: 15px 15px 0 0;
`;

class HeaderComponent extends Component {
  static propTypes = {
    scroll: PropTypes.number,
    children: PropTypes.node
  }

  state = {
    maxRadius: 100,
    radius: 100,
    minheight: 100,
    maxHeight: 250,
    height: 200
  }

  visibleInRect(percent) {
    return percent > 0 && percent < 100;
  }

  shouldComponentUpdate(nextProps, nextState) {
    const oldPercent = this.visibleInRect(this.state.percent);
    const newPercent = this.visibleInRect(nextState.percent);

    return nextState.percent !== this.state.percent && (newPercent || oldPercent);
  }

  static getDerivedStateFromProps(nextProps, nextState) {
    const { scroll } = nextProps;
    const { maxHeight, maxRadius, minheight } = nextState;
    const percent = scroll > maxHeight - minheight
      ? 0
      : scroll <= 0
        ? 100
        : 100 - (scroll * 100 / (maxHeight - minheight));

    if(scroll > maxHeight - minheight) {
      return {
        percent: percent,
        radius: 0,
        height: minheight
      };
    }else if(scroll <= 0) {
      return {
        percent: percent,
        radius: maxRadius,
        height: maxHeight
      };
    }else {
      return {
        height: maxHeight - scroll,
        radius: Math.round(maxRadius * percent / 100),
        percent: Math.round(percent)
      };
    }
  }

  render() {
    const { height, maxRadius, radius, maxHeight, minheight } = this.state;
    const { children } = this.props;

    return (
      <Wrapper
        top={0}
        height={maxHeight}>
        <Fixed
          height={maxHeight}>
          <Absolute
            top={-(maxHeight - height)}
            maxRadius={maxRadius}
            radius={radius}
            height={height}>
          </Absolute>
          <WrapperChildren
            height={minheight / 2}
            top={-(maxHeight - height)}>
            {children}
          </WrapperChildren>
        </Fixed>
      </Wrapper>
    );
  }
}

export default consumerScroll(HeaderComponent);
