import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tag } from 'antd';

class TagComponent extends Component {
  static propTypes = {
    meta: PropTypes.object
  }

  render() {
    const { meta } = this.props;
    let type = null;
    let color = null;

    if(meta.title) {
      type = 'link';
      color = 'purple';
    }

    if(/\.png|\.jpg|\.jpeg|\.ico|\.svg/.test(meta.title)) {
      type = 'cyan';
      color = 'image';
    }

    return <Tag color={color}>{type}</Tag>;
  }
}

export default TagComponent;
