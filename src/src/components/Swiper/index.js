import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Swipeable from 'react-swipeable';
import { Icon } from 'antd';
import transitionEnd from '../../Utils/transitionEnd';

const SwiperView = styled(Swipeable)`
  overflow: hidden;
  height: ${props => props.height}px;
  transition: height ${props => props.duration} ease-in;
`;

const Scrollview = styled.div`
  height: 100%;
  position: relative;
  width: 100%;
  overflow: hidden;
`;

const Scrollable = styled.div`
  transform: translate(${props => props.left}, 0);
  transition: transform ${props => props.duration} ease-in;
  position: relative;
  width: 100%;
  height: 100%;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
`;

const DeleteAction = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  background: ${props => props.theme.color.primary};
  background: linear-gradient(to right, ${props => props.theme.color.third} 0%, ${props => props.theme.color.fourth} 50%);
`;

const DeleteIcon = styled.div`
  font-size: ${props => props.size}px;
  transition: font-size .1s;
  color: white;
  position: absolute;
  top: 50%;
  transform: translate(-50%, -50%);
  left: ${props => props.max}px;
`;

class Swiper extends Component {
  static propTypes = {
    href: PropTypes.string,
    children: PropTypes.node,
    onDelete: PropTypes.func
  }

  state = {
    left: '0',
    height: 45,
    duration: '0s',
    afterTransitionSlide: this.afterTransitionSlide.bind(this),
    afterTransitionHeight: this.afterTransitionHeight.bind(this),
    swiperLeft: 100,
    maxIconSize: 25
  }

  reset() {
    this.setState({ left: '0', height: 45, duration: '0s' });
  }

  componentWillUpdate(nextProps) {
    if(nextProps.children !== this.props.children) this.reset();
  }

  swipingLeft(e, absX) {
    this.setState({ left: `${absX}px`, duration: '0s' });
  }

  afterTransitionHeight() {
    const { afterTransitionHeight } = this.state;
    const { onDelete } = this.props;

    this.swiperRef.element.removeEventListener(transitionEnd, afterTransitionHeight, false);
    this.swiperRef = null;
    onDelete && onDelete();
  }

  afterTransitionSlide() {
    const { afterTransitionSlide, afterTransitionHeight } = this.state;

    this.scrollRef.removeEventListener(transitionEnd, afterTransitionSlide, false);
    this.scrollRef = null;

    setTimeout(() => {
      this.swiperRef.element.addEventListener(transitionEnd, afterTransitionHeight, false);
      this.setState({ height: 0, duration: '0.2s' });
    }, 100);
  }

  onEnd(e, absX) {
    const { swiperLeft, afterTransitionSlide} = this.state;

    if(-absX > swiperLeft) {
      this.scrollRef.addEventListener(transitionEnd, afterTransitionSlide, false);
      this.setState({ left: '100%', duration: '0.2s' });
    }else {
      this.discard();
    }
  }

  discard() {
    this.setState({ left: '0px', duration: '0.5s' });
  }

  render() {
    const { children, href } = this.props;
    const { left, height, duration, swiperLeft, maxIconSize } = this.state;

    let iconSize = parseInt(left) * 100 / swiperLeft;
    iconSize = iconSize > 100 ? maxIconSize : iconSize * maxIconSize / 100;

    return (
      <SwiperView
        height={height}
        duration={duration}
        innerRef={(node) => this.swiperRef = node}
        onSwiped={() => this.discard()}
        onSwipedRight={(e, absx) => this.onEnd(e, absx)}
        onSwipingRight={(e, absx) => this.swipingLeft(e, absx)}>
        <Scrollview>
          <DeleteAction>
            <DeleteIcon
              max={maxIconSize}
              size={iconSize}>
              <Icon type="delete" />
            </DeleteIcon>
          </DeleteAction>
          <Scrollable
            innerRef={(node) => this.scrollRef = node}
            left={left}
            duration={duration}>
            <Link to={href}>
              {children}
            </Link>
          </Scrollable>
        </Scrollview>
      </SwiperView>
    );
  }
}

export default Swiper;
