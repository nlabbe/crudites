import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'antd';
import styled from 'styled-components';
import Swiper from '../Swiper';

const StyledList = styled(List.Item)`
  background: white;
  padding: 0 ${props => props.theme.layout.gutter}px;
  height: 100%;
  color: ${props => props.theme.color.primary};
`;

class DataListItem extends Component {
  static propTypes = {
    id: PropTypes.string,
    children: PropTypes.node,
    onDelete: PropTypes.func
  }

  render() {
    const { children, onDelete, id } = this.props;

    return (
      <Swiper
        href={`/${id}`}
        onDelete={() => onDelete && onDelete(id)}>
        <StyledList
          className="list-item">
          {children}
        </StyledList>
      </Swiper>
    );
  }
}

export default DataListItem;
