import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input, Form } from 'antd';
import styled from 'styled-components';


const StyledInput = styled(Input)`
  height: 50px;
  color: #000000;
  background: transparent;
  padding: 10px 20px;
  outline: none;
  border: none;
  box-shadow: none;
  :focus {
    outline: none;
    border: none;
    box-shadow: none;
  }
  ::-webkit-input-placeholder {
    color: #CCCCCC !important;
  }
  :-moz-placeholder { /* Firefox 18- */
    color: #CCCCCC !important;
  }
  ::-moz-placeholder {  /* Firefox 19+ */
    color: #CCCCCC !important;
  }
  :-ms-input-placeholder {
    color: #CCCCCC !important;
  }
`;

class InputSubmit extends Component {
  static propTypes = {
    submit: PropTypes.func
  }

  state = {
    value: ''
  }

  submit(e) {
    e.preventDefault();
    const { submit } = this.props;
    const { value } = this.state;

    submit && submit(value);
    this.setState({value: ''});
  }

  render() {
    return (
      <Form
        action="#"
        style={{
          position: 'relative',
          height: '100%'
        }} layout="inline" onSubmit={(e) => this.submit(e)}>
        <StyledInput
          placeholder="Add something..."
          onPressEnter={(e) => this.submit(e)}
          ref={(node) => this.inputRef = node}
          value={this.state.value}
          onChange={(e) => this.setState({value: e.currentTarget.value})} />
      </Form>
    );
  }
}

export default InputSubmit;
