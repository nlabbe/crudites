import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Card } from 'antd';

class DataItemDetail extends Component {
  static propTypes = {
    data: PropTypes.object
  }

  render() {
    const { data } = this.props;

    if(data && data.title) {
      return <Card
        className='card-detail'
        cover={data.image
          && <img style={{maxWidth: '100%',  margin: '0 auto', width: 'inherit'}}
            alt={data.title}
            src={data.image} />}>
        <Card.Meta
          title={<div style={{textAlign: 'center'}}>{data.title}</div>}
          description={<div style={{textAlign: 'center'}}>{data.description}</div>}
        />
        <Link to={data.url} target="_blank">{data.url}</Link>
      </Card>;
    }

    if(data && /\.png|\.jpg|\.jpeg|\.ico|\.svg/.test(data.title)) {
      return <img style={{maxWidth: '100%', margin: '0 auto', display: 'block'}} src={data.title} />;
    }

    return null;
  }
}

export default DataItemDetail;
