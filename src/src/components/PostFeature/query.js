import gql from 'graphql-tag';

export const POST = gql`
   query post($id: String!) {
    post(id: $id) {
        id
        date
        title
        meta
        parentId
      }
    }
 `;

export const POSTS = gql`
   query posts($parentId: String) {
    posts(parentId: $parentId) {
        id
        date
        title
        meta
        parentId
      }
    }
 `;

export const ADD_POST = gql`
  mutation addPost($title: String!, $parentId: String, $date: Date) {
    addPost(title: $title, parentId: $parentId, date: $date) {
      id
      date
      title
      meta
      parentId
    }
  }
`;

export const DELETE_POST = gql`
  mutation deletePost($id: String!) {
    deletePost(id: $id) {
      id
      date
      parentId
    }
  }
`;

export const DELETE_POSTS = gql`
  mutation deletePosts($ids: [String!]) {
    deletePosts(ids: $ids) {
      id
      parentId
    }
  }
`;

export const UPDATE_POST = gql`
  mutation updatePost($id: String!, $title: String, $date: Date) {
    updatePost(id: $id, title: $title, date: $date) {
      id
      title
      date
      meta
      parentId
    }
  }
`;
