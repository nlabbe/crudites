import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { ADD_POST, POSTS } from '../query';
import InputSubmit from '../../InputSubmit';

class PostAdd extends Component {
  static propTypes = {
    parentId: PropTypes.string,
    addPost: PropTypes.func
  }

  render() {
    const { parentId, addPost } = this.props;

    return (
      <InputSubmit
        submit={(value) => addPost(value, parentId, new Date())} />
    );
  }
}

const withAddPost = graphql(ADD_POST, {
  props: ({ mutate }) => ({
    addPost (title, parentId, date) {
      return mutate({
        variables: { title, parentId, date },
        update: (store, { data: { addPost } }) => {
          const { posts } = store.readQuery({
            query: POSTS,
            variables: { parentId: addPost.parentId }
          });
          posts.push(addPost);
          store.writeQuery({
            query: POSTS,
            variables: { parentId: addPost.parentId },
            data: { posts }
          });
        },
      });
    }
  })
});

export default withAddPost(PostAdd);
