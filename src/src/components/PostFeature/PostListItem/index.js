import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Col, notification } from 'antd';
import { List, Avatar } from 'antd';
import { graphql } from 'react-apollo';
import { UPDATE_POST } from '../query';
import Tag from '../../Tag';

class PostListItem extends Component {
  static propTypes = {
    addId: PropTypes.func,
    removeId: PropTypes.func,
    submit: PropTypes.func,
    id: PropTypes.string,
    data: PropTypes.object,
    updatePost: PropTypes.func
  }

  state = {
    id: '',
    title: '',
    meta: null
  }

  updateState(data) {
    this.setState({
      id: data.id,
      meta: data.meta && JSON.parse(data.meta),
      title: data.title
    });
  }

  componentDidMount() {
    this.updateState(this.props.data);
  }

  componentWillUpdate(nextProps) {
    if(nextProps.data !== this.props.data) this.updateState(nextProps.data);
  }

  update(value) {
    const { updatePost } = this.props;
    const { id } = this.state;
    if(value !== this.state.title) {
      updatePost(id, value);
    }
  }

  renderer(str) {
    return /\.png|\.jpg|\.jpeg|\.ico|\.svg/.test(str)
      ? <span><img style={{width: 32}} src={str} /> {str}</span>
      : str;
  }

  render() {
    const { title, meta, id } = this.state;

    return (
      <Col
        className="post-list"
        key={id}
        span={24}>
        {meta && meta.title
          ? <List.Item.Meta
            avatar={meta.image && <Avatar src={meta.image} />}
            title={meta.title && <a href={title}>{meta.title}</a>}
            description={meta.description && meta.description}
          />
          : title && this.renderer(title)}
        {meta && <Tag meta={meta} />}
      </Col>
    );
  }
}

const withUpdatePost = graphql(UPDATE_POST, {
  props: ({ mutate }) => ({
    updatePost (id, title) {
      return mutate({
        variables: { id, title },
        update: () => {
          notification.open({
            message: 'Successfully',
            description: 'saved',
            duration: 2
          });
        }
      });
    },
  }),
});

export default withUpdatePost(PostListItem);
