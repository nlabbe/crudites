import PostAdd from './PostAdd';
import PostList from './PostList';
import PostListItem from './PostListItem';
import Posts from './Posts';

export default {
  PostAdd,
  PostList,
  PostListItem,
  Posts
};
