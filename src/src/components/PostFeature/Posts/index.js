import { graphql } from 'react-apollo';
import PropTypes from 'prop-types';
import { POSTS } from '../query';
import React, { Component } from 'react';
import PostList from '../PostList';
import { Row, Col, Button } from 'antd';
import styled from 'styled-components';

const Wrapper = styled(Row)`
  background: white;
  margin: 0;
  z-index: 1;
  position: relative;
  border-radius: 15px 15px 0 0;
`;

class Posts extends Component {
  static propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.object,
    posts: PropTypes.array,
    scroll: PropTypes.number
  }

  state = {
    value: ''
  }

  render() {
    const { loading, error, posts} = this.props;

    if (error) return <p>{error.message}</p>;

    return (
      <Wrapper type="flex" justify="center">
        {loading && <Button type="primary" shape="circle" loading />}
        {error && <p>{error.message}</p>}
        {!loading && !error && <Col span={24}>
          <Col span={24}>
            <PostList
              posts={posts}
              afterRemove={() => this.refetch()} />
          </Col>
        </Col>}
      </Wrapper>
    );
  }
}

const withPosts = graphql(
  POSTS,
  {
    props: ({ data }) => {
      if (data.loading) return { loading: true };
      if (data.error) return { hasErrors: true };
      return {
        posts: data.posts,
      };
    }
  }
);

export default withPosts(Posts);
