import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DataList from '../../Datalist';
import DataListItem from '../../DataListItem';
import PostListItem from '../PostListItem';
import { graphql } from 'react-apollo';
import { DELETE_POST, POSTS } from '../query';
import styled from 'styled-components';

const StyledDataList = styled(DataList)`
  margin: 0;
`;

class PostList extends Component {
  static propTypes = {
    posts: PropTypes.array,
    checked: PropTypes.bool,
    toggleCheck: PropTypes.func,
    deletePost: PropTypes.func
  }

  getIds() {
    const { posts } = this.props;
    return posts.map((post) => post.id);
  }

  render() {
    const { posts, deletePost } = this.props;

    return (
      <span>
        <StyledDataList
          className="posts-list"
          bordered
          data={posts}
          render={item => <DataListItem
            id={item.id}
            onDelete={(id) => deletePost(id)}>
            <PostListItem
              data={item} />
          </DataListItem>}
        />
      </span>
    );
  }
}

const withDeletePost = graphql(DELETE_POST, {
  props: ({ mutate }) => ({
    deletePost (id) {
      return mutate({
        variables: { id },
        update: (store, { data: { deletePost: { id, parentId } } }) => {
          let { posts } = store.readQuery({
            query: POSTS,
            variables: { parentId }
          });
          posts = posts.filter(post => post.id !== id);
          store.writeQuery({
            query: POSTS,
            variables: { parentId },
            data: { posts }
          });
        }
      });
    },
  }),
});

export default withDeletePost(PostList);
