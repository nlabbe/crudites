import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'antd';
import styled from 'styled-components';

const StyledList = styled(List)`
  height: 100%;
`;

class DataList extends Component {
  static propTypes = {
    data: PropTypes.array,
    render: PropTypes.func
  }

  render() {
    const { data, render } = this.props;

    return (
      <StyledList
        dataSource={data}
        renderItem={item => render && render(item)}
      />
    );
  }
}

export default DataList;
