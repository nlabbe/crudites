export default {
  dark: '#000000',
  primary: '#5353e3',
  secondary: '#9150f5',
  third: '#ff8a54',
  fourth: '#fb48d4',
  light: '#FFFFFF'
};
