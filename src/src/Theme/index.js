import layout from './layout';
import font from './font';
import color from './color';

export default {
  layout,
  font,
  color
};
