import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
// import { persistCache } from 'apollo-cache-persist';

const cache = new InMemoryCache();

// persistCache({
//   cache,
//   storage: window.localStorage,
//   debug: true
// });

const client = new ApolloClient({
  link: new HttpLink({
    uri: '/graphql'
  }),
  connectToDevTools: true,
  cache: cache
});

export default client;
