import React from 'react';
import PropTypes from 'prop-types';
import PostFeature from '../components/PostFeature';
import Header from '../components/Header';

const PostsRoute = ({ parentId }) => <span>
  <Header>
    <PostFeature.PostAdd parentId={parentId} />
  </Header>
  <PostFeature.Posts parentId={parentId} />
</span>;

PostsRoute.propTypes = {
  parentId: PropTypes.string
};

export default PostsRoute;
