const el = document.createElement('div');
const transitions = {
  transition: 'transitionend',
  OTransition: 'otransitionend',  // oTransitionEnd in very old Opera
  MozTransition: 'transitionend',
  WebkitTransition: 'webkitTransitionEnd'
};

let name = false;
Object.keys(transitions).map((event) => {
  if(el.style[event] !== undefined) name = transitions[event];
});
export default name;
