import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import styled from 'styled-components';

import 'antd/dist/antd.css';

import Posts from './routes/Posts';
import { Scrollview } from './Context/scroll';

const Wrapper = styled.div`
  height: 100%;
  background: white;
`;

class App extends Component {

  render() {
    return (
      <Scrollview>
        <BrowserRouter>
          <Wrapper>
            <Route exact path='/:url?' render={(e) => <Posts parentId={e.match.params.url} />} />
          </Wrapper>
        </BrowserRouter>
      </Scrollview>
    );
  }
}

export default App;
