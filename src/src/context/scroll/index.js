import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  top: 0;
  left: 0;
`;

const ScrollContext = React.createContext({
  scroll: 0
});

export function consumerScroll(Component) {
  return function scrollComponent(props) {
    return (
      <ScrollContext.Consumer>
        {({ scroll }) =>
          <Component {...props} scroll={scroll} />}
      </ScrollContext.Consumer>
    );
  };
}

export class Scrollview extends Component {
  static propTypes = {
    children: PropTypes.node,
    style: PropTypes.object
  }

  state = {
    scroll: 0
  }

  componentDidMount() {
    if(this.scrollRef) {
      window.addEventListener('scroll', () => this.scroll());
    }
  }

  scroll() {
    const { scrollY } = window;
    if(scrollY >= 0) {
      this.setState({scroll: scrollY});
    }else if(this.state.scroll > 0) {
      this.setState({scroll: 0});
    }
  }

  render() {
    const { children, style } = this.props;

    return (
      <ScrollContext.Provider value={this.state}>
        <Wrapper
          style={style}
          innerRef={(node) => this.scrollRef = node}>
          {children}
        </Wrapper>
      </ScrollContext.Provider>
    );
  }
}

export default ScrollContext;
