import * as mongo from '../connector/mongo';
import * as filesystem from '../connector/filesystem';
import uuidv1 from 'uuid/v1';
import urlMetadata from 'url-metadata';

const connector = (process.env.NODE_ENV === 'production') ? mongo : filesystem;

connector.model('Post', {
  id: String,
  title: String,
  meta: String,
  date: Date,
  parentId: String
});

function scrape(title) {
  return new Promise((resolve) => {
    if(title.indexOf('http') > -1) {
      urlMetadata(title)
        .then(
          (metadata) => { // success handler
            resolve(JSON.stringify(metadata));
          },
          () => {
            resolve(null);
          });
    }else {
      resolve(null);
    }
  });
}

export const post = ({ id }) => {
  return connector.findOne('Post', {id: id});
}

export const posts = ({ parentId }) => {
  return connector.findChildren('Post', parentId);
}

export const addPost = ({ title, parentId, date }) => {
  return scrape(title)
    .then((meta) => connector.create('Post', {
      title: title,
      meta: meta,
      id: uuidv1(),
      date: date || new Date(),
      parentId: parentId || null
    }));
}

export const deletePost = ({ id }) => {
  return connector.deleteById('Post', id);
}

export const deletePosts = ({ ids }) => {
  return connector.deleteByIds('Post', ids);
}

export const updatePost = ({ id, title, date }) => {
  return scrape(title)
    .then((meta) => connector.updateById('Post', id, { title, date, meta }));
}
