import { buildSchema } from 'graphql';

export default buildSchema(`
    scalar Date,
    type Query {
      post(id: String!): Post
      posts(parentId: String): [Post]
    },
    type Post {
      id: String
      date: Date
      title: String
      meta: String
      parentId: String
    },
    type Mutation {
      updatePost(id: String!, title: String, date: Date): Post,
      addPost(title: String!, parentId: String, date: Date): Post,
      deletePost(id: String!): Post,
      deletePosts(ids: [String!]): [Post]
    },
    mutation ($title: title!, $parentId: String, $date: Date) {
      addPost(title: $title, parentId: $parentId, date: $date) {
        id
        date
        title
        meta
        parentId
      },
      deletePost(id: $id) {
        id
      },
      deletePosts(ids: $ids) {
        id
        parentId
      },
      updatePost(id: $id, title: $title, date: $date) {
        id
        date
        title
        meta
        parentId
      }
    },
    type Subscription {
      postAdded(repoFullName: String!): Post
    }
`);
