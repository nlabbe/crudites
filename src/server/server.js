import path from 'path';
import express from 'express';
import express_graphql from 'express-graphql';

// GraphQL schema
import schema from './schema/schema.js';
import * as resolver from './schema/resolver.js';

// Create an express server and a GraphQL endpoint
const app = express();
app.use('/graphql', express_graphql({
  schema: schema,
  rootValue: resolver,
  graphiql: true
}));

if(process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, '../client')));
}

app.use('/db.json', (req, res, next) => {
  res.sendFile(path.join(process.cwd(), 'storage', 'db.json'));
});

app.listen(
  process.env.PORT || 4000,
  () => console.log(`Express GraphQL Server Now Running On localhost:${process.env.PORT || 4000}/graphql`)
);
