const mongoose = require('mongoose');

const uri = 'mongodb://crudites:crudites@ds063630.mlab.com:63630/crudites';

const options = {
  poolSize: 2,
  promiseLibrary: global.Promise
};

mongoose.connect(uri, options);

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

const models = [];

export const model = (name, json) => {
  models[name] = mongoose.model(name, json, name);
}

const deleteRecursive = (name, id) => {
  return new Promise((resolve, reject) => {
    models[name].find({ parentId: id }, (err, children) => {
      const promises = [];
      children && children.map((child) => {
        promises.push(deleteRecursive(child.id));
      });

      Promise.all(promises)
        .then(() => {
          models[name].findOne({id: id}, (err, doc) => {
            if (err) return reject(err);
            models[name].remove({id: id}).exec(() => resolve(doc))
          });
        })
      });
  });
}

export const findOne = (name, where) => {
  return new Promise((resolve, reject) => {
    models[name].findOne(where, (err, doc) => {
      resolve(doc)
    });
  });
}

export const findChildren = (name, parentId) => {
  return new Promise((resolve, reject) => {
    models[name].find({parentId: parentId}, (err, docs) => {
      resolve(docs || []);
    });
  });
}

export const create = (name, obj) => {
  return new Promise((resolve) => {
    const doc = new models[name](obj);

    doc.save()
      .then(() => {
        resolve(doc);
      });
  });
}

export const deleteById = (name, id) => {
  return new Promise((resolve) => {
    deleteRecursive(name, id)
      .then((deletedPost) => resolve({
        id: deletedPost.id,
        parentId: deletedPost.parentId || null
      }));
  });
}

export const updateById = (name, id, data) => {
  return new Promise((resolve, reject) => {
    models[name].findOneAndUpdate({id: id}, data, {upsert: true}, function(err, doc){
      if (err) return reject(err);
      return resolve(doc);
    });
  });
}
