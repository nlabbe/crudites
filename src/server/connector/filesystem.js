import path from 'path';
import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';
import uuidv1 from 'uuid/v1';
import mkdirp from 'mkdirp';

mkdirp(path.join(process.cwd(), 'storage'));
const adapter = new FileSync(path.join(process.cwd(), 'storage', 'db.json'));
const db = low(adapter);

export const model = (name, json) => {
  db.defaults({ [name]: [] }).write();
}

const deleteRecursive = (name, id) => {
    const children = db.get(name)
        .filter({parentId: id})
        .value()

    children && children.map((child) => {
        deleteRecursive(child.id);
    });
    const post = db.get(name)
      .find({ id: id })
      .value();
    if(post) {
      db.get(name)
        .remove({ id: id })
        .write();
    }
    return post;
}

export const findOne = (name, where) => {
    return new Promise((resolve) => {
      const post = db.get(name)
        .find(where)
        .value();
      resolve(post);
    });
}

export const findChildren = (name, parentId) => {
  return new Promise((resolve, reject) => {
    let posts = db.get(name)
      .value();

    posts = posts.filter(post => parentId
      ? post.parentId === parentId
        ? post
        : false
      : post.parentId === null
        ? post
        : false);

    resolve(posts);
  });
}

export const create = (name, obj) => {
  return new Promise((resolve) => {
    db.get(name)
      .push(obj)
      .write();
    resolve(obj);
  });
}

export const deleteById = (name, id) => {
  return new Promise((resolve) => {
    const deletedPost = deleteRecursive(name, id);
    resolve({
      id: deletedPost.id,
      parentId: deletedPost.parentId || null
    });
  });
}

export const deleteByIds = (name, ids) => {
  return new Promise((resolve) => {
    const promises = [];
    const returnIds = [];
    ids && ids.map((id) => {
      return promises.push(new Promise((resolve) => {
        deleteById(name, id)
          .then((data) => {
            returnIds.push(data);
            resolve();
          });
      }));
    });
    Promise.all(promises)
      .then(() => {
        resolve(returnIds);
      });
  });
}

export const updateById = (name, id, data) => {
  return new Promise((resolve) => {
    resolve(db.get(name)
      .find({ id: id })
      .assign(data)
      .write());
  });
}
